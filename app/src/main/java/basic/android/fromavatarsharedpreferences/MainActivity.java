package basic.android.fromavatarsharedpreferences;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.IOException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText edtName, edtFamily, edtTel;
    Button btnTakeImage, btnTakeImageGallery, btnSave;
    ImageView imgAvatar;
    Context mContext = this;
    Activity mActivity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindingViews();
        setListeners();
        loadDataFromShared();
    }

    public void bindingViews() {
        imgAvatar = findViewById(R.id.imgAvatar);
        btnTakeImage = findViewById(R.id.btnTakeImage);
        btnTakeImageGallery = findViewById(R.id.btnTakeImageGallery);

        edtName = findViewById(R.id.edtName);
        edtFamily = findViewById(R.id.edtFamily);
        edtTel = findViewById(R.id.edtTel);

        btnSave = findViewById(R.id.btnSave);
    }

    public void setListeners() {
        btnTakeImage.setOnClickListener(this);
        btnTakeImageGallery.setOnClickListener(this);
        btnSave.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view == btnTakeImage) {
            isCameraPermissionEnabled();
        } else if (view == btnTakeImageGallery) {
            isGalleryPermissionEnabled();
        } else if (view == btnSave) {
            setDataToSharedPreferences();
        }

    }

    public void isGalleryPermissionEnabled() {
        try {
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 300);
                Log.d("my log", "1");
            } else {
                Log.d("my log", "2");
                openGallery();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void isCameraPermissionEnabled() {

        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            openCameraToTakePicture();
        } else {
            ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.CAMERA}, 200);
        }

    }

    public void openGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, 350);
    }

    public void openCameraToTakePicture() {

        Intent intentCaptureImage = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intentCaptureImage, 250);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 250) {
            if (resultCode == Activity.RESULT_OK) {
                Bitmap avatar = (Bitmap) data.getExtras().get("data");
                Glide.with(mActivity).load(avatar).into(imgAvatar);
            }
        } else if (requestCode == 350) {
            Log.d("my log", "3");
            if (resultCode == Activity.RESULT_OK) {
                Log.d("my log", "4");

                Uri uri = data.getData();

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                    Glide.with(mActivity).load(bitmap).into(imgAvatar);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 200) {

            Toast.makeText(mContext, "مجاز به ثبت عکس نیستید", Toast.LENGTH_LONG).show();

        }
    }

    public void setDataToSharedPreferences() {
        setShared("edtName", edtName.getText().toString());
        setShared("edtFamily", edtFamily.getText().toString());
        setShared("edtTel", edtTel.getText().toString());

        edtName.setText("");
        edtFamily.setText("");
        edtTel.setText("");
    }

    public void setShared(String key, String value) {
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(key, value).apply();
    }

    public String getShared(String key, String defValue) {
        return PreferenceManager.getDefaultSharedPreferences(mContext).getString(key, defValue);
    }

    public void loadDataFromShared() {
        edtName.setText(getShared("edtName", ""));
        edtFamily.setText(getShared("edtFamily", ""));
        edtTel.setText(getShared("edtTel", ""));
    }

}

